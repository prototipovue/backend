<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banks', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        DB::table('banks')->insert([
            [ 'id' => 1, 'name' => 'Bancolombia', 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            [ 'id' => 2, 'name' => 'Banco de Bogotá', 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            [ 'id' => 3, 'name' => 'BBVA', 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banks');
    }
}
