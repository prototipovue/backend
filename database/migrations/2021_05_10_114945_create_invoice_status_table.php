<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_status', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        DB::table('invoice_status')->insert([
            [ 'id' => 1, 'name' => 'Por pagar', 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            [ 'id' => 2, 'name' => 'Por confirmar', 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            [ 'id' => 3, 'name' => 'Pago confirmado', 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            [ 'id' => 9, 'name' => 'Anulada', 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_status');
    }
}
