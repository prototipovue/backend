<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Hash;

class AddPersonalDatesToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('document');
            $table->string('phone');
            $table->integer('status')->default(1);
            $table->unsignedBigInteger('user_type_id')->default(2);
            $table->foreign('user_type_id')->references('id')->on('user_types');
        });

        DB::table('users')->insert([
            'id' => 1, 'name' => 'Admin', 'email' => 'admin@mail.com', 'document' => '12345678', 'user_type_id' => 1, 'phone' => '0',
            'password' => Hash::make('admin'), 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('user_type_id');
            $table->dropColumn('user_type_id');
            $table->dropColumn('document');
            $table->dropColumn('phone');
            $table->dropColumn('status');
        });
    }
}
