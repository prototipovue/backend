<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_types', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        DB::table('user_types')->insert([
            [ 'id' => 1, 'name' => 'Administrador', 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            [ 'id' => 2, 'name' => 'Cliente', 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_types');
    }
}
