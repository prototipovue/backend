<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_types', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('status')->default(1);
            $table->timestamps();
        });

        DB::table('product_types')->insert([
            [ 'id' => 1, 'name' => 'Producto', 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            [ 'id' => 2, 'name' => 'Servicio', 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_types');
    }
}
