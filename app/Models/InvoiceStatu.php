<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvoiceStatu extends Model
{
    use HasFactory;

    const TO_BE_PAY = 1;
    const TO_BE_CONFIRMED = 2;
    const PAYMENT_CONFIRMED = 3;
    const CANCELED = 9;
}