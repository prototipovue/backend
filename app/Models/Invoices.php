<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Invoices extends Model
{
    use HasFactory;

    protected $appends = [
        'total'
    ];

    protected $fillable = [
        'user_id','date','status_id'
    ];

    public function user() {
        return $this->belongsTo(User::class,'user_id');
    }

    public function bank() {
        return $this->belongsTo(Banks::class,'bank_id');
    }

    public function status() {
        return $this->belongsTo(InvoiceStatu::class,'status_id');
    }

    public function details() {
        return $this->hasMany(InvoiceDetails::class,'invoice_id');
    }

    public function getTotalAttribute() {
        return $this->details()->sum(DB::raw("price*quantity"));
    }
}
