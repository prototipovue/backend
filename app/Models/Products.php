<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'status','product_type_id','price'
    ];

    protected $appends = [
        'status_text'
    ];

    const ACTIVATED = 1;
    const DEACTIVATED = 0;

    public function product_type() {
        return $this->belongsTo(ProductTypes::class,'product_type_id');
    }

    public function getStatusTextAttribute(){
        return $this->status == 1 ? 'Activado' : 'Desactivado';
    }
}
