<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvoiceDetails extends Model
{
    use HasFactory;

    protected $fillable = [
        'invoice_id','product_id','price', 'quantity'
    ];

    public function product() {
        return $this->belongsTo(Products::class,'product_id');
    }
    
}
