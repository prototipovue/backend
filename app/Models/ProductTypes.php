<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductTypes extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'status'
    ];

    protected $appends = [
        'status_text'
    ];

    const ACTIVATED = 1;
    const DEACTIVATED = 0;

    public function getStatusTextAttribute(){
        return $this->status == 1 ? 'Activado' : 'Desactivado';
    }
    
}
