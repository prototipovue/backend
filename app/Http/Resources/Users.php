<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Users extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'document' => $this->document,
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'user_type_id' => $this->user_type_id,
            'user_type' => $this->user_type,
            'status' => !is_null($this->status) ? $this->status : 1,
            'status_text' => $this->status == 1 ? 'Activado' : 'Desactivado', 
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
