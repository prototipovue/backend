<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Products extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'product_type_id' => $this->product_type_id,
            'price' => $this->price,
            'status' => !is_null($this->status) ? $this->status : 1,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
