<?php

namespace App\Http\Controllers\api;

use App\Models\Products;
use App\Models\ProductTypes;
use App\Http\Controllers\api\BaseController as BaseController;
use Illuminate\Http\Request;
use App\Http\Resources\Products as ProductsResource;
use Validator;
use DB;

class ProductsController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $itemsPerPage=$request->itemsPerPage ? $request->itemsPerPage : 10;
        $products = Products::with('product_type');
        if($request->search){
            $products->orWhere('name','like','%'.$request->search.'%');
            $products->orWhere(DB::raw('CAST(price AS CHAR)'), 'like', '%'.$request->search.'%');
            $products->orWhereHas('product_type', function ($q) use ($request) {
                $q->where('name','like','%'.$request->search.'%');
            });
        }

        if(isset($request->sortBy) && count($request->sortBy) > 0){
            $field = $request->sortBy[0];
            $order = $request->sortDesc[0] == 'false' ? 'ASC' : 'DESC';
            $products->orderBy($field,$order);
        }
        $products = $products->paginate($itemsPerPage);
    
        return $this->sendResponse($products, 'Listado de productos exitoso');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
   
        $validator = Validator::make($input, [
            'name' => 'required',
            'product_type_id' => 'required'
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
   
        $product = Products::create($input);
   
        return $this->sendResponse(new ProductsResource($product), 'Producto creado exitosamente');
    } 
   
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Products::find($id);
  
        if (is_null($product)) {
            return $this->sendError('Producto no encontrado');
        }
   
        return $this->sendResponse(new ProductsResource($product), 'Producto consultado exitosamente');
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Products $product)
    {
        $input = $request->all();
        $product->fill($input);
        $product->save();
   
        return $this->sendResponse(new ProductsResource($product), 'Producto salvado exitosamente');
    }
   
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Products $product)
    {
        $product->delete();
        return $this->sendResponse([], 'Producto eliminado exitosamente');
    }

    public function deactivate($id)
    {
        $product = Products::find($id);
        if (is_null($product)) {
            return $this->sendError('Producto no encontrado');
        }

        if ($product->status == Products::DEACTIVATED) {
            return $this->sendError('Este producto ya se encuentra desactivado');
        }

        $product->status = Products::DEACTIVATED;
        $product->save();

        return $this->sendResponse(new ProductsResource($product), 'Producto desactivado exitosamente');
    }

    public function activate($id)
    {
        $product = Products::find($id);
        if (is_null($product)) {
            return $this->sendError('Producto no encontrado');
        }

        if ($product->status == Products::ACTIVATED) {
            return $this->sendError('Este producto ya se encuentra activado');
        }

        $product->status = Products::ACTIVATED;
        $product->save();

        return $this->sendResponse(new ProductsResource($product), 'Producto activado exitosamente');
    }

    public function getReferences(Request $request)
    {
        $product_types = ProductTypes::where('status',ProductTypes::ACTIVATED)->get();
        $references = [
            'product_types' => $product_types
        ];
        return $this->sendResponse($references, 'Listado de referencias exitoso');
    }
}
