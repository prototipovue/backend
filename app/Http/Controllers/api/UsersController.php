<?php

namespace App\Http\Controllers\api;

use App\Models\User;
use App\Models\UserTypes;
use App\Http\Controllers\api\BaseController as BaseController;
use Illuminate\Http\Request;
use App\Http\Resources\Users as UsersResource;
use Validator;

class UsersController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $itemsPerPage=$request->itemsPerPage ? $request->itemsPerPage : 10;
        $users = User::with('user_type');
        if($request->search){
            $users->where('document','like','%'.$request->search.'%');
            $users->orWhere('name','like','%'.$request->search.'%');
            $users->orWhere('email','like','%'.$request->search.'%');
            $users->orWhere('phone','like','%'.$request->search.'%');
            $users->orWhereHas('user_type', function ($q) use ($request) {
                $q->where('name','like','%'.$request->search.'%');
            });
        }

        if(isset($request->sortBy) && count($request->sortBy) > 0){
            $field = $request->sortBy[0];
            $order = $request->sortDesc[0] == 'false' ? 'ASC' : 'DESC';
            $users->orderBy($field,$order);
        }
        $users = $users->paginate($itemsPerPage);

        return $this->sendResponse($users, 'Listado de usuarios exitoso');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
   
        $validator = Validator::make($input, [
            'document' => 'required',
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'user_type_id' => 'required'
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
   
        $user = User::create($input);
   
        return $this->sendResponse(new UsersResource($user), 'Usuario creado exitosamente');
    } 
   
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
  
        if (is_null($user)) {
            return $this->sendError('Usuario no encontrado');
        }
   
        return $this->sendResponse(new ProductsResource($product), 'Usuario consultado exitosamente');
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $input = $request->all();
   
        $user->document = $input['document'];
        $user->name = $input['name'];
        $user->email = $input['email'];
        $user->phone = $input['phone'];
        $user->user_type_id = $input['user_type_id'];
        $user->save();
   
        return $this->sendResponse(new UsersResource($user), 'Usuario editado exitosamente');
    }
   
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return $this->sendResponse([], 'Usuario eliminado exitosamente');
    }

    public function deactivate($id)
    {
        $user = User::find($id);
        if (is_null($user)) {
            return $this->sendError('Usuario no encontrado');
        }

        if ($user->status == User::DEACTIVATED) {
            return $this->sendError('Este usuario ya se encuentra desactivado');
        }

        $user->status = User::DEACTIVATED;
        $user->save();

        return $this->sendResponse(new UsersResource($user), 'Usuario desactivado exitosamente');
    }

    public function activate($id)
    {
        $user = User::find($id);
        if (is_null($user)) {
            return $this->sendError('Usuario no encontrado');
        }

        if ($user->status == User::ACTIVATED) {
            return $this->sendError('Este usuario ya se encuentra activado');
        }

        $user->status = User::ACTIVATED;
        $user->save();

        return $this->sendResponse(new UsersResource($user), 'usuario activado exitosamente');
    }

    public function getReferences(Request $request)
    {
        $user_types = UserTypes::get();
        $references = [
            'user_types' => $user_types
        ];
        return $this->sendResponse($references, 'Listado de referencias exitoso');
    }
}
