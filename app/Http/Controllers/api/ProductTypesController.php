<?php
   
namespace App\Http\Controllers\api;
   
use Illuminate\Http\Request;
use App\Http\Controllers\api\BaseController as BaseController;
use App\Models\ProductTypes;
use Validator;
use App\Http\Resources\ProductTypes as ProductTypesResource;
   
class ProductTypesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $itemsPerPage=$request->itemsPerPage ? $request->itemsPerPage : 10;
        $product_types = ProductTypes::whereIn('status',[0,1]);
        if($request->search){
            $product_types->where('name','like','%'.$request->search.'%');
        }

        if(isset($request->sortBy) && count($request->sortBy) > 0){
            $field = $request->sortBy[0];
            $order = $request->sortDesc[0] == 'false' ? 'ASC' : 'DESC';
            $product_types->orderBy($field,$order);
        }
        $product_types = $product_types->paginate($itemsPerPage);
    
        return $this->sendResponse($product_types, 'Listado de tipos de productos exitoso');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
   
        $validator = Validator::make($input, [
            'name' => 'required',
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
   
        $product_type = ProductTypes::create($input);
   
        return $this->sendResponse(new ProductTypesResource($product_type), 'Tipo de producto creado exitosamente');
    } 
   
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product_type = ProductTypes::find($id);
  
        if (is_null($product_type)) {
            return $this->sendError('Tipo de producto no encontrado');
        }
   
        return $this->sendResponse(new ProductTypesResource($product_type), 'Tipo de producto consultado exitosamente');
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductTypes $product_type)
    {
        $input = $request->all();
   
        $validator = Validator::make($input, [
            'name' => 'required'
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
   
        $product_type->name = $input['name'];
        $product_type->save();
   
        return $this->sendResponse(new ProductTypesResource($product_type), 'Tipo de Producto salvado exitosamente');
    }
   
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductTypes $product_type)
    {
        $product_type->delete();
        return $this->sendResponse([], 'Tipo de producto eliminado exitosamente');
    }

    public function deactivate($id)
    {
        $product_type = ProductTypes::find($id);
        if (is_null($product_type)) {
            return $this->sendError('Tipo de producto no encontrado');
        }

        if ($product_type->status == ProductTypes::DEACTIVATED) {
            return $this->sendError('Este tipo de producto ya se encuentra desactivado');
        }

        $product_type->status = ProductTypes::DEACTIVATED;
        $product_type->save();

        return $this->sendResponse(new ProductTypesResource($product_type), 'Tipo de Producto desactivado exitosamente');
    }

    public function activate($id)
    {
        $product_type = ProductTypes::find($id);
        if (is_null($product_type)) {
            return $this->sendError('Tipo de producto no encontrado');
        }

        if ($product_type->status == ProductTypes::ACTIVATED) {
            return $this->sendError('Este tipo de producto ya se encuentra activado');
        }

        $product_type->status = ProductTypes::ACTIVATED;
        $product_type->save();

        return $this->sendResponse(new ProductTypesResource($product_type), 'Tipo de Producto activado exitosamente');
    }


}