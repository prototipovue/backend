<?php

namespace App\Http\Controllers\api;

use App\Models\Invoices;
use App\Models\InvoiceDetails;
use App\Models\User;
use App\Models\Products;
use App\Models\Banks;
use App\Http\Controllers\api\BaseController as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
//use App\Http\Resources\Invoices as InvoicesResource;
use Validator;
use DB;

class InvoicesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $itemsPerPage=$request->itemsPerPage ? $request->itemsPerPage : 10;
        $invoices = Invoices::with(['user','status','details.product.product_type', 'bank']);

        if($request->user_id){
            $invoices->where('user_id',$request->user_id);
        }

        if($request->search){
            $invoices->whereHas('user', function ($q) use ($request) {
                $q->where('name','like','%'.$request->search.'%');
            });
        }

        if(isset($request->sortBy) && count($request->sortBy) > 0){
            $field = $request->sortBy[0];
            $order = $request->sortDesc[0] == 'false' ? 'ASC' : 'DESC';
            $products->orderBy($field,$order);
        }
        $invoices = $invoices->paginate($itemsPerPage);
    
        return $this->sendResponse($invoices, 'Listado de facturas exitoso');
    }

    public function store(Request $request)
    {
        $input = $request->all();

        try {
            \DB::beginTransaction();

            $input['invoice']['date'] = date("Y-m-d");
            $input['invoice']['status_id'] = 1;
            $invoice = Invoices::create($input['invoice']);

            foreach ($input['invoice_details'] as $detail) {
                $detail['invoice_id'] = $invoice->id;
                $invoice_detail = InvoiceDetails::create($detail);
            }

            \DB::commit();
            return $this->sendResponse($invoice, 'Factura generada exitosamente');

        } catch (Throwable $e) {
            \DB::rollback();
            return $this->sendError('Error generando factura');
        }
    }

    public function getUsers(Request $request)
    {
        $users = 
            User::where('status',1)
                ->where(function($q) use ($request) {
                    $q->where('name','like','%'.$request->search.'%')
                        ->orWhere('document','like','%'.$request->search.'%');
                })
                ->limit(20)
                ->get();
    
        return $this->sendResponse($users, 'Listado de clientes exitoso');
    }

    public function getProducts(Request $request)
    {
        $users = 
            Products::with(['product_type'])
                ->where('status',1)
                ->where('name','like','%'.$request->search.'%')
                ->limit(20)
                ->get();
    
        return $this->sendResponse($users, 'Listado de productos exitoso');
    }

    public function getReferences(Request $request)
    {
        $banks = Banks::get();
        $references = [
            'banks' => $banks
        ];
        return $this->sendResponse($references, 'Listado referencias exitoso');
    }

    public function cancel($id)
    {
        $invoice = Invoices::find($id);
        if (is_null($invoice)) {
            return $this->sendError('Factura no encontrada');
        }

        if ($invoice->status_id == 9) {
            return $this->sendError('Esta factura ya fue cancelada');
        }

        $invoice->status_id = 9;
        $invoice->save();

        return $this->sendResponse($invoice, 'Factura anulada exitosamente');
    }

    public function payment($id, Request $request)
    {
        $invoice = Invoices::find($id);
        if (is_null($invoice)) {
            return $this->sendError('Factura no encontrada');
        }

        if ($invoice->status_id == 2) {
            return $this->sendError('Esta factura ya fue reportado su pago');
        }

        $invoice->bank_id = $request->bank_id;
        $invoice->reference = $request->reference;
        $invoice->status_id = 2;
        $invoice->save();

        return $this->sendResponse($invoice, 'Pago reportado exitosamente');
    }

    public function paymentConfirm($id, Request $request)
    {
        $invoice = Invoices::find($id);
        if (is_null($invoice)) {
            return $this->sendError('Factura no encontrada');
        }

        if ($invoice->status_id == 3) {
            return $this->sendError('Esta factura ya fue confirmado su pago');
        }

        $invoice->status_id = 3;
        $invoice->save();

        return $this->sendResponse($invoice, 'Pago confirmado exitosamente');
    }
    
}
