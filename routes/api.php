<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\api\RegisterController;
use App\Http\Controllers\api\ProductTypesController;
use App\Http\Controllers\api\ProductsController;
use App\Http\Controllers\api\UsersController;
use App\Http\Controllers\api\InvoicesController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [RegisterController::class, 'register']);
Route::post('login', [RegisterController::class, 'login']);

Route::middleware('auth:api')->group( function () {
    Route::resource('product-type', ProductTypesController::class);
    Route::post('product-type/activate/{id}', [ProductTypesController::class, 'activate']);
    Route::post('product-type/deactivate/{id}', [ProductTypesController::class, 'deactivate']);

    Route::resource('product', ProductsController::class);
    Route::post('product/get-references', [ProductsController::class, 'getReferences']);
    Route::post('product/activate/{id}', [ProductsController::class, 'activate']);
    Route::post('product/deactivate/{id}', [ProductsController::class, 'deactivate']);

    Route::resource('invoice', InvoicesController::class);
    Route::post('invoice/get-users', [InvoicesController::class, 'getUsers']);
    Route::post('invoice/get-products', [InvoicesController::class, 'getProducts']);
    Route::post('invoice/get-references', [InvoicesController::class, 'getReferences']);
    Route::post('invoice/cancel/{id}', [InvoicesController::class, 'cancel']);
    Route::post('invoice/payment/{id}', [InvoicesController::class, 'payment']);
    Route::post('invoice/payment-confirm/{id}', [InvoicesController::class, 'paymentConfirm']);
    

    Route::resource('user', UsersController::class);
    Route::post('user/get-references', [UsersController::class, 'getReferences']);
    Route::post('user/activate/{id}', [UsersController::class, 'activate']);
    Route::post('user/deactivate/{id}', [UsersController::class, 'deactivate']);
});
